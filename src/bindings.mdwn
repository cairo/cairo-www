[[!meta title="Language bindings"]]

Cairo bindings, which are expected to follow the [binding
guidelines][1] as closely as possible, are available for various
languages:

  * Ada : [CairoAda][15]

  * C++ : [[cairomm]]

  * Common Lisp : [cl-cairo2](http://cliki.net/cl-cairo2), [cffi-cairo](http://www.cliki.net/cffi-cairo)

  * COM-Wrapper : aka "ActiveX-dll" ... easiest to use with VB5/6 or VBA,
    comes with cairo as a "satellite-dll" (compiled with StdCall-callingconvention)
    [download-page:](http://www.thecommon.net/3.html) (a large VB-Democode-Tutorial is available there too)

  * Harbour : [hbcairo](http://sourceforge.net/projects/harbour-project/)

  * Haskell : [[hscairo]]

  * Java : [[org.freedesktop.cairo|cairo-java]] in java-gnome

  * C# : [[https://github.com/zwcloud/CairoSharp]]

    * Mono and .NET bindings used to be bundled with the Mono
      distribution in the [Mono.Cairo][2] library, but this is no longer
      maintained.
    * At one point, bindings had also been provided at
      [NDesk](http://www.ndesk.org/).

  * [Nickle][3] : [[cairo-nickle]]

  * Objective Caml : [[cairo-ocaml]]

  * Perl : [[cairo-perl]]

  * PHP : [[cairo-php]]

  * Prolog : [PLcairo][25]

  * Python : [[pycairo]],
    [qahirah][20] high-level Pythonic binding, [cairocffi][21] binding created with
    CFFI, [pygobject][22] includes Cairo binding.

  * Ruby : [[rcairo]]

  * Scheme: [guile-cairo][4]

  * [Ypsilon Scheme](http://code.google.com/p/ypsilon) ships with Cairo bindings

  * [Squeak][5] : [Rome][6]

  * Digitalmars D: [cairoD][7] - simple wrapper for D direct C API

  * [Lua][11] : [Lua-Cairo][12], [Lua-OOCairo][13], [LuaCairo][16]

  * [LuaJIT][23]: [luapower/cairo][24]

  * [Vala][14]

# Pixman Bindings

  * Python : [python_pixman][17] is a high level pixel-manipulation
    library ([examples][18])

# Toolkit Bindings

Since cairo is only a drawing library, it can be quite useful to
integrate it with a graphical user interface toolkit.

  * [FLTK][8] has full cairo support (through "--enable-cairo" compile
    switch).

  * [GNUstep][9] bindings: <http://cvs.savannah.gnu.org/viewvc/gnustep/gnustep/core/back/Source/cairo/>

  * [GTK+][10] 2.8+ has full support for cairo.

   [1]: http://www.cairographics.org/manual/language-bindings.html
   [2]: http://www.mono-project.com/Drawing#Mono.Cairo
   [3]: http://nickle.org/
   [4]: http://home.gna.org/guile-cairo/
   [5]: http://www.squeak.org/
   [6]: http://www.squeaksource.com/Rome/
   [7]: http://www.dsource.org/projects/bindings/wiki/CairoGraphics/
   [8]: http://www.fltk.org/
   [9]: http://gnustep.org/
   [10]: http://gtk.org/
   [11]: http://www.lua.org/
   [12]: http://luaforge.net/projects/luacairo/
   [13]: http://www.daizucms.org/lua/library/oocairo/
   [14]: http://live.gnome.org/Vala
   [15]: http://sourceforge.net/projects/cairoada/
   [16]: http://www.dynaset.org/dogusanh/
   [17]: https://github.com/ldo/python_pixman/
   [18]: https://github.com/ldo/python_pixman_examples/
   [19]: https://github.com/ldo/pycairo
   [20]: https://github.com/ldo/qahirah
   [21]: https://pythonhosted.org/cairocffi/
   [22]: https://wiki.gnome.org/action/show/Projects/PyGObject
   [23]: http://www.luajit.org/
   [24]: http://luapower.com/cairo
   [25]: https://github.com/keriharris/plcairo/
