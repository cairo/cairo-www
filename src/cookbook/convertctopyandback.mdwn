Converting cairo code between languages is very easy.

##C to Python

the following C code:

	cairo_new_path (cr);
	cairo_move_to (cr, x + radius_x, y);

to Python:

	cr.new_path()
	cr.move_to (x + radius_x, y)

* replace "`cairo_`" with "`cr.`"
* remove "`cr`" as first argument

Note that in some instances the python bindings will try to simplify, and then a direct translation is not possible. In such a case refer to the source code of [[pycairo]], it is relatively easy to read.

##Haskell

to Haskell:

        newPath
        moveTo (x + radius_x) y


