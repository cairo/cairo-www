[[!meta title="Objective Caml bindings"]]

[Objective Caml](http://caml.inria.fr/) bindings are in the
[cairo-ocaml](http://cgit.freedesktop.org/cairo-ocaml/) module of the git repository.

The following [[backends]] are supported:

  * image (via the Bigarray module)

  * ps

  * pdf

  * xlib (via the [GTK+](http://www.gtk.org/) interface
    [LablGTK](http://wwwfun.kurims.kyoto-u.ac.jp/soft/olabl/lablgtk.html))

  * svg

There are also bindings for the SVG renderer
[libsvg-cairo](http://cairographics.org/snapshots/libsvg-cairo-0.1.6.tar.gz).

# Releases

Version 1.0.0 of cairo-ocaml targets the 1.0.x releases of Cairo. It
has tag cairo-ocaml-1.0.0 in the git repository.

The head of the repository requires Cairo >= 1.2.x.

# Documentation

See the modules
[signatures](http://oandrieu.nerim.net/ocaml/cairo/doc/index.html) in
ocamldoc format.

# A very simple example

This is a simple example, (ported from a [[pycairo]] example):

    let width  = 400
    let height = 400

    let main =
      (* Setup Cairo *)
      let surface = Cairo.image_surface_create Cairo.FORMAT_ARGB32 ~width ~height in
      let ctx = Cairo.create surface in

      (* Set thickness of brush *)
      Cairo.set_line_width ctx 15. ;

      (* Draw out the triangle using absolute coordinates *)
      Cairo.move_to     ctx   200.  100. ;
      Cairo.line_to     ctx   300.  300. ;
      Cairo.rel_line_to ctx (-200.)   0. ;
      Cairo.close_path  ctx ;

      (* Apply the ink *)
      Cairo.stroke ctx ;

      (* Output a PNG file *)
      Cairo_png.surface_write_to_file surface "triangle.png"

# Projects using cairo-ocaml

 * [Caboodle](http://juripakaste.fi/caboodle/), a clone of the Flash game Planarity

 * [GeoProof](http://home.gna.org/geoproof/), an interactive geometry software with proof related features
