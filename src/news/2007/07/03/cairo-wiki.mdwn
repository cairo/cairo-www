[[!meta title="New cairo wiki available, please help!"]]
[[!meta date="2007-07-03"]]

I'm happy to announce that we've just gone live with a new cairo wiki
at:

<http://cairographics.org>

This is an [ikiwiki][1]-based wiki that replaces the MoinMoin-based wiki
that was [lost][2] a couple of months ago, (it also replaces the static-
HTML placeholder we had in place in the interim). Here are some of the
benefits of the new setup:

* RSS/Atom feeds for news items, automatic update of front page with
  latest news.

* Syntax is more sane, (uses [MarkDown][3] which uses either
  \_underscores_ or \*asterisks* to indicate _emphasis_ rather than
  insane ''italics'' and '''bold''').

* Completely integrated with git, meaning:

  * The distributed nature of git gives us another level of redundancy
    in backups, (so hopefully we'll never lose our wiki content again).

  * Standard git tools work for browsing history

  * Regular contributors can just "git push" changes without
    using any obnoxious web interface, (but the drive-by user
    can use the web-interface and know nothing of git).

* Templates and CSS for theming are right there alongside the
  source in the git repository, (not locked away in some database
  requiring the obnoxious web interface to edit them)

We encourage anybody to start editing the website to improve it. Add
your tips and tricks, things you've learned about cairo, and things
you're confused about. I'd also be glad to have people contribute
larger scale reorganizations and cleanups. The wiki requires only the
most painless registration step imaginable, and then allows web-based
editing by anyone willing to go through that registration step.

Now that we've got this new wiki in place, here are some projects that
I would like to see happen:

* Resurrect the old samples (cairo-snippets) and get them nicely
  integrated with the wiki

* Similarly, get all the old cairo-demo stuff into the wiki

* Put the cairo documentation into the wiki where people can, (if
  not directly edit and improve it), at least click on a
  "discussion" link for any given page to indicate where there are
  any questions about any items.

  Our documentation has never really grown past simply being API
  function reference, and I'd really like to see some real
  reference material and tutorials get written. I'd also like to
  expand the set of output targets we're getting. I know we're
  currently publishing HTML and at least devhelp from gtk-doc, and
  I'd definitely like to get man pages and PDF output as well.

  Maybe we can do all of that with gtk-doc, but it might be easier
  to shift to something like [AsciiDoc][4] instead.

If you (like me) would rather not ever use a web form for editing web
content, then you can get the cairo wiki source via git through one of
the following URLs:

	git://cairographics.org/git/cairo-www

	git+ssh://cairographics.org/git/cairo-www

where the second requires a freedesktop.org account, but allows users
in the cairo group to push changes out via git.

Recently, there have been a lot of very useful things floating by on
the mailing list, and I'd love

You may notice that the look and feel of the website has changed.
That's partly just to be different and partly to help indicate that
things have changed. If you find the new look tacky, you can blame my
utter lack of web-design talent and you may consider this an
invitation to help fix things. One huge benefit compared to our
MoinMoin setup is that we now have much easier mechanisms for
controlling templates, etc. and everything is done in a much cleaner
fashion, (semantic HTML with CSS styling, etc.). For example, the
navigation bar at the top of each page is really nothing more than an
unordered list as far as the HTML is concerned, then it's styled with
CSS to have the horizontal look.

Currently, changes to the cairo website will be mailed out to the
cairo-commit mailing list. The wiki is supposed to have its own
mechanism for subscribing for notifications to specific pages, but it
doesn't seem to be working yet.

Finally, I'd like to extend thanks to Joey Hess for writing ikiwiki
and for being very receptive with questions from me, (and accepting a
few patches as well), as I put this together.

-Carl

[1]: http://ikiwiki.info

[2]: http://lists.cairographics.org/archives/cairo/2007-May/010594.html

[3]: http://daringfireball.net/projects/markdown/syntax

[4]: http://www.methods.co.nz/asciidoc/